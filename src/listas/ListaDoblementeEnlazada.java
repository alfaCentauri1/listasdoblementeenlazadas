package listas;

public class ListaDoblementeEnlazada implements Lista{
    private Nodo primero;
    private Nodo ultimo;

    private int tamanio;

    public ListaDoblementeEnlazada() {
        primero = null;
        ultimo = null;
        tamanio = 0;
    }


    /**
     * Agrega el dato al principio de la lista.
     *
     * @param dato Tipo int.
     * @return Regresa la lista.
     **/
    public void agregar(int dato) {
        Nodo nuevo = new Nodo(dato);
        if (tamanio == 0){
            primero = nuevo;
            ultimo = nuevo;
        }
        else{
            nuevo.setAnterior(ultimo);
            ultimo.setSiguiente(nuevo);
            ultimo = nuevo;
        }
        tamanio++;
    }

    /**
     * Agrega el dato al principio de la lista.
     *
     * @param dato Tipo int.
     * @return Regresa la lista.
     **/
    @Override
    public Lista agregarInicio(int dato) {
        Nodo nuevo = new Nodo(dato);
        if (tamanio == 0){
            primero = nuevo;
            ultimo = nuevo;
        }
        else {
            nuevo.setSiguiente(primero);
            primero.setAnterior(nuevo);
            primero = nuevo;
        }
        tamanio++;
        return this;
    }

    /**
     * Agrega el dato al final de la lista.
     *
     * @param dato Tipo int.
     **/
    @Override
    public void agregarFinal(int dato) {
        Nodo nodoFin = new Nodo(dato);
        nodoFin.setSiguiente(null);
        if (primero != null){
            Nodo temporal;
            for ( temporal = primero; temporal.getSiguiente() != null; temporal = temporal.getSiguiente() );
            temporal.setSiguiente(nodoFin);
        }
        else {
            primero = nodoFin;
        }
    }

    /**
     * Encuentra un nodo con un dato específico.
     *
     * @param dato Tipo int.
     * @return
     */
    @Override
    public Nodo buscar(int dato) {
        Nodo encontrado = null, actual;
        for(actual = primero; actual != null; actual = actual.getSiguiente()){
            if (dato == actual.getDato())
                encontrado = actual;
        }
        return encontrado;
    }

    /**
     * @param dato Tipo int.
     * @return
     */
    @Override
    public boolean eliminar(int dato) {
        boolean encontrado = false;
        Nodo nodoActual = primero;
        Nodo nodoAnterior = null;
        while (nodoActual != null && !encontrado){
            encontrado = (nodoActual.getDato() == dato);
            if (!encontrado){
                nodoAnterior = nodoActual;
                nodoActual = nodoActual.getSiguiente();
            }
        }
        if (nodoActual != null){
            if (nodoActual == primero){
                primero = nodoActual.getSiguiente();
            }
            else {
                Nodo siguiente = nodoActual.getSiguiente();
                nodoAnterior.setSiguiente(siguiente);
                siguiente.setAnterior(nodoAnterior);
            }
        }
        return encontrado;
    }

    public boolean eliminarFinal(){
        var encontrado = true;
        if (tamanio > 0){
            if (tamanio == 1){
                primero = null;
                ultimo = null;
            }
            else {
                Nodo actual = ultimo.getAnterior();
                actual.setSiguiente(null);
                ultimo.setSiguiente(null);
                ultimo.setAnterior(null);
                ultimo = actual;
            }
            tamanio--;
        }
        else
            encontrado = false;

        return encontrado;
    }

    public boolean eliminarInicio(){
        boolean encontrado = false;
        Nodo nodoActual = primero;
        if (tamanio > 0) {
            if (tamanio == 1) {
                primero = null;
                ultimo = null;
            } else {
                primero = nodoActual.getSiguiente();
                primero.setAnterior(null);
                nodoActual = null;
            }
        }
        return encontrado;
    }

    /**
     * Imprime la lista.
     */
    @Override
    public void verLista() {
        System.out.println("La lista doblemente enlazada es: ");
        Nodo nodoActual = primero;
        while (nodoActual != null){
            System.out.println(nodoActual.getDato());
            nodoActual = nodoActual.getSiguiente();
        }
    }

    public Nodo getPrimero() {
        return primero;
    }

    public void setPrimero(Nodo primero) {
        this.primero = primero;
    }
}
