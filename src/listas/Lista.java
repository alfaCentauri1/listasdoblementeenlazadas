package listas;

public interface Lista {
    /**
     * Agrega el dato al principio de la lista.
     * @param dato Tipo int.
     * @return Regresa la lista.
     **/
    Lista agregarInicio(int dato);
    /**
     * Agrega el dato al final de la lista.
     * @param dato Tipo int.
     **/
    void agregarFinal(int dato);

    /**
     * Encuentra un nodo con un dato específico.
     * @param dato Tipo int.
     * @return
     */
    public Nodo buscar(int dato);

    /**
     * Borra un nodo indicado por el dato.
     * @param dato Tipo int.
     * @return Regresa verdadero si realiza la operación, sino regresa falso.
     */
    boolean eliminar(int dato);
    void verLista();
}
