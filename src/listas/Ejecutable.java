package listas;

public class Ejecutable {
    public static  void main(String args[]){
        ListaDoblementeEnlazada listaDoblementeEnlazada = new ListaDoblementeEnlazada();
        listaDoblementeEnlazada.agregar(1);
        listaDoblementeEnlazada.agregar(2);
        listaDoblementeEnlazada.agregar(3);
        listaDoblementeEnlazada.agregar(5);
        listaDoblementeEnlazada.agregar(7);
        listaDoblementeEnlazada.agregar(4);
        listaDoblementeEnlazada.agregarInicio(6);
        listaDoblementeEnlazada.eliminarFinal();
        listaDoblementeEnlazada.eliminarInicio();
        listaDoblementeEnlazada.eliminar(3);
        listaDoblementeEnlazada.verLista();
        System.out.println("Buscando un dato ");
        Nodo encontrado = listaDoblementeEnlazada.buscar(8);
        if(encontrado != null) {
            System.out.println("El dato encontrado es: " + encontrado.getDato());
        }
        else {
            System.out.println("No se encontró el dato.");
        }
    }
}
